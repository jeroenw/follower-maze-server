# Solution Following Maze challenge 

There are 3 main processes

- One is listening for incoming users

    Reading of the user id is done asynchronously in a new thread, this prevents the process of blocking when nothing is received
    
- One is listening for incoming events sources

    To not block to process, a new thread will be launched for every event source that is received (depending on the configuration value of _maxEventConnections_. Here every event is given to the _EventProcessor_.

- One to process the received events

    Will process the next event, dependent of the sequence number, when available.

## Requirements

- JDK 8 JVM

## Build

~~~console
./gredlew jar
~~~

## Run

From current directory:

~~~console
java -jar build/libs/follower-maze-server-1.0.jar
~~~

Or the jar file can be found in _./build/libs/_

## Configuration

It is possible to set the configuration of the program via environment variables:

- **eventListenerPort** default: 9090

    Port used for event source

- **clientListenerPort** default: 9099
    
    Port used for user clients
        
-  **logLevel**  default: INFO
    
    available: SEVERE, WARNING, INFO, FINE
    
- **maxEventConnections** default: 1

    set maximum amount of source event connections