package org.jw.followermaze.user;

import org.junit.Test;
import org.jw.followermaze.server.ConnectionHandler;
import org.jw.followermaze.server.user.User;
import org.jw.followermaze.server.user.UserBucket;
import org.jw.followermaze.server.user.IncomingUserHandler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IncomingUserHandlerTest {
    private UserBucket bucket = new UserBucket();
    private Socket socket = mock(Socket.class);

    private ConnectionHandler handler = new IncomingUserHandler(bucket);

    private void test(String socketInput) throws IOException {
        when(socket.getInputStream()).thenReturn( new ByteArrayInputStream(socketInput.getBytes()));
        when(socket.getOutputStream()).thenReturn(System.out);

        handler.handle(socket);
    }

    @Test
    public void testNewIncomingConnection() throws IOException {
        test("1");

        User user = bucket.get(1);

        assertNotNull(user);
        assertThat(user.getUserId(), equalTo(1));
    }

    @Test
    public void testNewIncomingConnectionWithWrongInput() throws IOException {
        test("a");

        Collection<User> users = bucket.getAll();

        assertTrue(users.isEmpty());
    }
}
