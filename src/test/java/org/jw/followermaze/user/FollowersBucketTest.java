package org.jw.followermaze.user;

import org.junit.Before;
import org.junit.Test;
import org.jw.followermaze.server.user.FollowersBucket;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class FollowersBucketTest {
    private FollowersBucket bucket;

    @Before
    public void setUp() {
        bucket = new FollowersBucket();
        bucket.addFollower(1, 10);
        bucket.addFollower(1, 11);
        bucket.addFollower(2, 20);
    }

    @Test
    public void testGettingFollowersOf() {
        Set<Integer> followers = bucket.getFollowersOf(1);

        Set<Integer> expected = new HashSet<>();
        expected.add(10);
        expected.add(11);

        assertThat(followers.size(), is(2));
        assertThat(followers, is(expected));
    }

    @Test
    public void testAddingDuplicateFollowerFor() {
        bucket.addFollower(1, 10);
        testGettingFollowersOf();
    }

    @Test
    public void testRemoveFollower() {
        bucket.removeFollower(1, 11);

        Set<Integer> followers = bucket.getFollowersOf(1);

        Set<Integer> expected = new HashSet<>();
        expected.add(10);


        assertThat(followers.size(), is(1));
        assertThat(followers, is(expected));
    }

    @Test
    public void testRemoveInvalidFollower() {
        bucket.removeFollower(1, 100);

        Set<Integer> followers = bucket.getFollowersOf(1);

        Set<Integer> expected = new HashSet<>();
        expected.add(10);
        expected.add(11);

        assertThat(followers.size(), is(2));
        assertThat(followers, is(expected));
    }

    @Test
    public void testRemoveFollowerFrom() {
        bucket.removeFollower(3, 100);

        Map<Integer, Set<Integer>> followers = bucket.getAll();

        Set<Integer> first = new HashSet<>();
        first.add(10);
        first.add(11);
        Set<Integer> second = new HashSet<>();
        second.add(20);
        Map<Integer, Set<Integer>> expected = new HashMap<>();
        expected.put(1, first);
        expected.put(2, second);

        assertThat(followers, is(expected));
    }
}
