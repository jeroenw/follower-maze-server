package org.jw.followermaze.user;

import org.junit.Test;
import org.jw.followermaze.server.user.User;

import java.io.PrintWriter;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class UserTest {
    private PrintWriter writer = mock(PrintWriter.class);
    private User user = new User(1, writer);

    @Test
    public void testWrite() {
        String payload = "666|F|60|50";

        user.write(payload);
        verify(writer).println(payload);
    }

    @Test
    public void testGetId() {
        assertThat(user.getUserId(), equalTo(1));
    }
}
