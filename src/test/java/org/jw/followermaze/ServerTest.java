package org.jw.followermaze;

import org.junit.Before;
import org.junit.Test;
import org.jw.followermaze.server.ConnectionHandler;
import org.jw.followermaze.server.Server;

import java.io.IOException;
import java.net.Socket;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ServerTest {
    private ConnectionHandler handler = mock(ConnectionHandler.class);
    private int port = 9090;

    @Before
    public void setUp() throws IOException {
        Server server = new Server(port, handler);
        new Thread(server).start();
    }

    @Test
    public void testHandleNewConnections() throws IOException {
        new Socket("localhost", port);
        verify(handler, timeout(1000).times(1)).handle(any(Socket.class));
    }
}
