package org.jw.followermaze.events;

import org.junit.Before;
import org.junit.Test;
import org.jw.followermaze.server.events.EventListener;
import org.jw.followermaze.server.events.EventProcessor;
import org.jw.followermaze.server.events.event.BroadcastEvent;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.Socket;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.timeout;

public class EventListenerTest {
    private EventProcessor processor = mock(EventProcessor.class);

    @Before
    public void setUp() throws IOException {
        BufferedReader reader = mock(BufferedReader.class);
        when(reader.readLine()).thenReturn("1|B").thenReturn(null);

        new Thread(new EventListener(reader, processor)).start();
    }

    @Test
    public void receiveIncomingBroadcastEvents() {
        verify(processor, timeout(1000).times(1)).addEvent(any(BroadcastEvent.class));
    }
}
