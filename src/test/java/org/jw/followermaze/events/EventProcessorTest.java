package org.jw.followermaze.events;

import org.junit.Test;
import org.jw.followermaze.server.events.EventProcessor;
import org.jw.followermaze.server.events.event.*;
import org.jw.followermaze.server.user.UserBucket;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.timeout;

public class EventProcessorTest {
    @Test
    public void routeToCorrectProcess() {
        Event follow = mock(FollowEvent.class);
        Event unfollow = mock(UnfollowEvent.class);
        Event broadcast = mock(BroadcastEvent.class);
        Event privateMessage = mock(PrivateMessageEvent.class);
        Event statusUpdate = mock(StatusUpdateEvent.class);

        when(follow.getSequence()).thenReturn(1);
        when(unfollow.getSequence()).thenReturn(2);
        when(broadcast.getSequence()).thenReturn(3);
        when(privateMessage.getSequence()).thenReturn(4);
        when(statusUpdate.getSequence()).thenReturn(5);

        EventProcessor processor = spy(new EventProcessor(mock(UserBucket.class)));
        new Thread(processor).start();

        processor.addEvent(follow);
        processor.addEvent(unfollow);
        processor.addEvent(broadcast);
        processor.addEvent(privateMessage);
        processor.addEvent(statusUpdate);

        verify(processor, timeout(1000).times(5)).addEvent(any(Event.class));
        verify(processor, timeout(1000).times(5)).process(any(Event.class));
        verify(processor, timeout(1000).times(1)).follow(any(FollowEvent.class));
        verify(processor, timeout(1000).times(1)).unfollow(any(UnfollowEvent.class));
        verify(processor, timeout(1000).times(1)).broadcast(any(BroadcastEvent.class));
        verify(processor, timeout(1000).times(1)).privateMessage(any(PrivateMessageEvent.class));
        verify(processor, timeout(1000).times(1)).statusUpdate(any(StatusUpdateEvent.class));
    }
}
