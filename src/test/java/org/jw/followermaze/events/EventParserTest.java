package org.jw.followermaze.events;

import org.junit.Test;
import org.jw.followermaze.server.events.EventParser;
import org.jw.followermaze.server.events.event.*;
import org.jw.followermaze.server.events.exceptions.InvalidPayloadException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

public class EventParserTest {
    private final EventParser factory = new EventParser();

    @Test
    public void testCreateValidFollowEvent() throws InvalidPayloadException {
        final String payload = "666|F|60|50";
        FollowEvent event = (FollowEvent)factory.parse(payload);

        assertThat(event, instanceOf(Event.class));
        assertThat(event, instanceOf(FollowEvent.class));

        assertThat(event.getPayload(), equalTo(payload));
        assertThat(event.getSequence(), equalTo(666));
        assertThat(event.getFromUserId(), equalTo(60));
        assertThat(event.getToUserId(), equalTo(50));
    }

    @Test
    public void testCreateValidUnfollowEvent() throws InvalidPayloadException {
        final String payload = "1|U|12|9";
        UnfollowEvent event = (UnfollowEvent)factory.parse(payload);

        assertThat(event, instanceOf(Event.class));
        assertThat(event, instanceOf(UnfollowEvent.class));

        assertThat(event.getPayload(), equalTo(payload));
        assertThat(event.getSequence(), equalTo(1));
        assertThat(event.getFromUserId(), equalTo(12));
        assertThat(event.getToUserId(), equalTo(9));
    }

    @Test
    public void testCreateValidBroadcastEvent() throws InvalidPayloadException {
        final String payload = "542532|B";
        BroadcastEvent event = (BroadcastEvent)factory.parse(payload);

        assertThat(event, instanceOf(Event.class));
        assertThat(event, instanceOf(BroadcastEvent.class));

        assertThat(event.getPayload(), equalTo(payload));
        assertThat(event.getSequence(), equalTo(542532));
    }

    @Test
    public void testCreateValidPrivateMessageEvent() throws InvalidPayloadException {
        final String payload = "43|P|32|56";
        PrivateMessageEvent event = (PrivateMessageEvent)factory.parse(payload);

        assertThat(event, instanceOf(Event.class));
        assertThat(event, instanceOf(PrivateMessageEvent.class));

        assertThat(event.getPayload(), equalTo(payload));
        assertThat(event.getSequence(), equalTo(43));
        assertThat(event.getFromUserId(), equalTo(32));
        assertThat(event.getToUserId(), equalTo(56));
    }

    @Test
    public void testCreateValidStatusUpdateEvent() throws InvalidPayloadException {
        final String payload = "634|S|32";
        StatusUpdateEvent event = (StatusUpdateEvent)factory.parse(payload);

        assertThat(event, instanceOf(Event.class));
        assertThat(event, instanceOf(StatusUpdateEvent.class));

        assertThat(event.getPayload(), equalTo(payload));
        assertThat(event.getSequence(), equalTo(634));
        assertThat(event.getFromUserId(), equalTo(32));
    }

    @Test(expected = InvalidPayloadException.class)
    public void testInvalidInputLength1() throws InvalidPayloadException {
        factory.parse("345");
    }

    @Test(expected = InvalidPayloadException.class)
    public void testInvalidInputLength2() throws InvalidPayloadException {
        factory.parse("345|F|435|345|345");
    }

    @Test(expected = InvalidPayloadException.class)
    public void testInvalidEventType() throws InvalidPayloadException {
        factory.parse("345|Q|435|345");
    }

    @Test(expected = InvalidPayloadException.class)
    public void testInvalidEventTypeLength() throws InvalidPayloadException {
        factory.parse("634|S|32|45");
    }

    @Test(expected = InvalidPayloadException.class)
    public void testInvalidNumberFormat() throws InvalidPayloadException {
        factory.parse("634|F|T|45");
    }
}
