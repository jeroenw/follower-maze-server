package org.jw.followermaze.events;

import org.junit.Test;
import org.jw.followermaze.server.events.EventProcessor;
import org.jw.followermaze.server.events.event.BroadcastEvent;
import org.jw.followermaze.server.events.event.Event;
import org.jw.followermaze.server.user.UserBucket;

import static org.mockito.Mockito.*;

public class EventProcessorStressTest {
    private EventProcessor processor;

    private void processEvents(int total) {
        processor = spy(new EventProcessor(mock(UserBucket.class)));
        new Thread(processor).start();

        for (int i = 1; i <= total; i++) {
            Event event = mock(BroadcastEvent.class);
            when(event.getSequence()).thenReturn(i);
            processor.addEvent(event);
        }

    }

    @Test
    public void process1000Events() {
        processEvents(1000);
        verify(processor, timeout(1000).times(1000)).addEvent(any(Event.class));
        verify(processor, timeout(1000).times(1000)).process(any(Event.class));
    }

    @Test
    public void process100kEvents() {
        processEvents(100000);
        verify(processor, timeout(1000).times(100000)).addEvent(any(Event.class));
        verify(processor, timeout(1000).times(100000)).process(any(Event.class));
    }

    @Test
    public void process500kEvents() {
        processEvents(500000);
        verify(processor, timeout(1000).times(500000)).addEvent(any(Event.class));
        verify(processor, timeout(1000).times(500000)).process(any(Event.class));
    }

    @Test
    public void process1mEvents() {
        processEvents(1000000);
        verify(processor, timeout(1000).times(1000000)).addEvent(any(Event.class));
        verify(processor, timeout(1000).times(1000000)).process(any(Event.class));
    }

    @Test
    public void process10MEvents() {
        processEvents(10000000);
        verify(processor, timeout(1000).times(1000000)).addEvent(any(Event.class));
        verify(processor, timeout(1000).times(1000000)).process(any(Event.class));
    }

}
