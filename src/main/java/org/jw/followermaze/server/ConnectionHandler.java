package org.jw.followermaze.server;

import java.net.Socket;

public interface ConnectionHandler {
    boolean acceptNewConnections();
    void handle(Socket socket);
    void shutdown();
}
