package org.jw.followermaze.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketTimeoutException;
import java.util.logging.Logger;

public class Server extends Worker {
    private final int port;
    private final ServerSocket serverSocket;
    private final ConnectionHandler handler;

    private final static Logger logger = Logger.getLogger(Server.class.getName());

    public Server(int port, ConnectionHandler handler) throws IOException {
        this.port = port;
        this.serverSocket = new ServerSocket(port);
        this.serverSocket.setSoTimeout(1000);
        this.handler = handler;
    }

    @Override
    public void run() {
        while (!shutdown.get() && handler.acceptNewConnections()) {
            try {
                handler.handle(serverSocket.accept());
            } catch (SocketTimeoutException e) {
            } catch (IOException e) {
                logger.severe(String.format("Failed to accept incoming connection on %d", port));
                e.printStackTrace();
            }
        }
    }

    @Override
    public void shutdown() {
        handler.shutdown();
        super.shutdown();
    }
}
