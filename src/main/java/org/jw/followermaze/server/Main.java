package org.jw.followermaze.server;

import org.jw.followermaze.server.config.Configuration;
import org.jw.followermaze.server.config.EnvConfiguration;
import org.jw.followermaze.server.events.EventConnectionHandler;
import org.jw.followermaze.server.events.EventProcessor;
import org.jw.followermaze.server.user.IncomingUserHandler;
import org.jw.followermaze.server.user.UserBucket;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private final static Logger logger = Logger.getLogger(Main.class.getName());

    private static void setUpLogging(Level logLevel) {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tc [%4$s] %5$s %n");

        Logger root = Logger.getLogger("");
        root.setLevel(logLevel);
        for (Handler handler : root.getHandlers()) {
            handler.setLevel(logLevel);
        }
    }

    public static void main(String[] args) throws IOException {
        Configuration conf = new EnvConfiguration();
        setUpLogging(conf.getLogLevel());

        UserBucket userBucket = new UserBucket();
        EventProcessor processor = new EventProcessor(userBucket);
        ConnectionHandler eventConnectionHandler = new EventConnectionHandler(processor, conf.getMaxEventConnections());
        Server eventServer = new Server(conf.getEventSourcePort(), eventConnectionHandler);

        ConnectionHandler userConnectionHandler = new IncomingUserHandler(userBucket);
        Server userServer = new Server(conf.getClientPort(), userConnectionHandler);

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.submit(processor);
        executorService.submit(eventServer);
        executorService.submit(userServer);

        // Properly end all threads
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                processor.shutdown();
                userServer.shutdown();
                eventServer.shutdown();

                executorService.shutdown();
                try {
                    if(!executorService.awaitTermination(5, TimeUnit.SECONDS)) {
                        logger.severe("Failed to close all processes, will force");
                    }
                } catch (InterruptedException e) {
                    logger.severe("Failed to close all processes, will force");
                }
            }
        });


    }
}
