package org.jw.followermaze.server.events;

import org.jw.followermaze.server.ConnectionHandler;
import org.jw.followermaze.server.Worker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class EventConnectionHandler implements ConnectionHandler {
    private final int maxConnections;
    private final EventProcessor processor;
    private ExecutorService executorService;
    private List<Worker> cleanupPool = new ArrayList<>();

    private final static Logger logger = Logger.getLogger(EventConnectionHandler.class.getName());

    public EventConnectionHandler(EventProcessor processor, int maxConnections) {
        this.maxConnections = maxConnections;
        this.processor = processor;
        executorService = Executors.newFixedThreadPool(maxConnections);
    }

    @Override
    public boolean acceptNewConnections() {
        return cleanupPool.size() < maxConnections;
    }

    @Override
    public void handle(Socket socket) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            EventListener listener = new EventListener(reader, processor);
            cleanupPool.add(listener);
            executorService.submit(listener);

            logger.info("Source connection established, start listening for events");
        } catch (IOException e) {
            logger.severe("Failed to start listening for events on connected source connection");
        }
    }

    @Override
    public void shutdown() {
        for (Worker w : cleanupPool) {
            w.shutdown();
        }
        executorService.shutdown();

        try {
            if (!executorService.awaitTermination(5, TimeUnit.SECONDS)) {
                logger.severe("Failed to close Event Listeners, will force");
            }
        } catch (InterruptedException e) {
            logger.severe("Failed to close Event Connection Handler");
        }
    }
}
