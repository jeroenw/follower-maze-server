package org.jw.followermaze.server.events.event;

public class StatusUpdateEvent extends Event {
    private final int fromUserId;

    public StatusUpdateEvent(String payload, int sequence, int fromUserId) {
        super(payload, sequence);
        this.fromUserId = fromUserId;
    }

    public int getFromUserId() {
        return fromUserId;
    }
}

