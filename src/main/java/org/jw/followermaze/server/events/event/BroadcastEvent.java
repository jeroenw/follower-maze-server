package org.jw.followermaze.server.events.event;

public class BroadcastEvent extends Event {
    public BroadcastEvent(String payload, int sequence) {
        super(payload, sequence);
    }
}

