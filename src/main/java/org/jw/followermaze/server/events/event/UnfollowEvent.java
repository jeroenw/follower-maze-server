package org.jw.followermaze.server.events.event;

public class UnfollowEvent extends Event {
    private final int fromUserId;
    private final int toUserId;

    public UnfollowEvent(String payload, int sequence, int fromUserId, int toUserId) {
        super(payload, sequence);
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
    }

    public int getFromUserId() {
        return fromUserId;
    }

    public int getToUserId() {
        return toUserId;
    }
}

