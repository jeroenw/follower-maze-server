package org.jw.followermaze.server.events.event;

public class PrivateMessageEvent extends Event {
    private final int fromUserId;
    private final int toUserId;

    public PrivateMessageEvent(String payload, int sequence, int fromUserId, int toUserId) {
        super(payload, sequence);
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
    }

    public int getFromUserId() {
        return fromUserId;
    }

    public int getToUserId() {
        return toUserId;
    }
}
