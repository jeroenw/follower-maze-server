package org.jw.followermaze.server.events;

import org.jw.followermaze.server.events.event.*;
import org.jw.followermaze.server.events.exceptions.InvalidPayloadException;

public class EventParser {
    public Event parse(String payload) throws InvalidPayloadException {
        final String[] parts = payload.split("\\|");

        try {
            int sequence = Integer.parseInt(parts[0]);
            String type = parts[1];

            switch (type) {
                case "F":
                    if (parts.length != 4) break;
                    return new FollowEvent(payload, sequence, Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
                case "U":
                    if (parts.length != 4) break;
                    return new UnfollowEvent(payload, sequence, Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
                case "B":
                    if (parts.length != 2) break;
                    return new BroadcastEvent(payload, sequence);
                case "P":
                    if (parts.length != 4) break;
                    return new PrivateMessageEvent(payload, sequence, Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
                case "S":
                    if (parts.length != 3) break;
                    return new StatusUpdateEvent(payload, sequence, Integer.parseInt(parts[2]));
            }
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
            throw new InvalidPayloadException();
        }

        throw new InvalidPayloadException();
    }
}
