package org.jw.followermaze.server.events.event;

public abstract class Event {
    private final String payload;
    private final int sequence;

    public Event(String payload, int sequence) {
        this.payload = payload;
        this.sequence = sequence;
    }

    public String getPayload() {
        return payload;
    }

    public int getSequence() {
        return sequence;
    }
}

