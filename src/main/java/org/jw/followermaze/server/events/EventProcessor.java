package org.jw.followermaze.server.events;

import org.jw.followermaze.server.Worker;
import org.jw.followermaze.server.events.event.*;
import org.jw.followermaze.server.user.FollowersBucket;
import org.jw.followermaze.server.user.User;
import org.jw.followermaze.server.user.UserBucket;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class EventProcessor extends Worker {
    private int currentSequence = 1;
    private ConcurrentHashMap<Integer, Event> events = new ConcurrentHashMap<>();
    private UserBucket users;
    private FollowersBucket followers = new FollowersBucket();

    private final static Logger logger = Logger.getLogger(EventProcessor.class.getName());

    public EventProcessor(UserBucket users) {
        this.users = users;
    }

    public void addEvent(Event event) {
        this.events.put(event.getSequence(), event);
    }

    @Override
    public void run() {
        while (!shutdown.get()) {
            if (events.containsKey(currentSequence)) {
                Event nextEvent = events.get(currentSequence);

                logger.fine(String.format("Processing event sequence %d: %s, event.size: %d", currentSequence, nextEvent.getPayload(), events.size()));

                process(nextEvent);
                events.remove(currentSequence);
                currentSequence++;
            }
        }
    }

    public void process(Event event) {
        if (event instanceof FollowEvent) {
            follow((FollowEvent) event);
        } else if (event instanceof UnfollowEvent) {
            unfollow((UnfollowEvent) event);
        } else if (event instanceof BroadcastEvent) {
            broadcast((BroadcastEvent) event);
        } else if (event instanceof PrivateMessageEvent) {
            privateMessage((PrivateMessageEvent) event);
        } else if (event instanceof StatusUpdateEvent) {
            statusUpdate((StatusUpdateEvent) event);
        } else {
            logger.warning(String.format("Event with sequence number %d could not be processed", event.getSequence()));
        }
    }

    private void notifyAll(Event event) {
        for (User user : users.getAll()) {
            user.write(event.getPayload());
        }
    }

    private void notifyUserClient(User user, Event event) {
        if (user != null) {
            user.write(event.getPayload());
        }
    }

    private void notifyUserClient(int userClientId, Event event) {
        User user = users.get(userClientId);
        notifyUserClient(user, event);
    }

    public void follow(FollowEvent event) {
        followers.addFollower(event.getToUserId(), event.getFromUserId());
        notifyUserClient(event.getToUserId(), event);
    }

    public void unfollow(UnfollowEvent event) {
        followers.removeFollower(event.getToUserId(), event.getFromUserId());
    }

    public void broadcast(BroadcastEvent event) {
        notifyAll(event);
    }

    public void privateMessage(PrivateMessageEvent event) {
        User user = users.get(event.getToUserId());
        notifyUserClient(user, event);
    }

    public void statusUpdate(StatusUpdateEvent event) {
        Set<Integer> userClientIds = followers.getFollowersOf(event.getFromUserId());
        if (userClientIds != null && !userClientIds.isEmpty()) {
            for (int userClientId : userClientIds) {
                notifyUserClient(userClientId, event);
            }
        }
    }
}
