package org.jw.followermaze.server.events;

import org.jw.followermaze.server.Worker;
import org.jw.followermaze.server.events.exceptions.InvalidPayloadException;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Logger;

public class EventListener extends Worker {
    private final BufferedReader reader;
    private final EventParser parser;
    private final EventProcessor processor;

    private final static Logger logger = Logger.getLogger(EventListener.class.getName());

    public EventListener(BufferedReader reader, EventProcessor processor) {
        this.parser = new EventParser();
        this.reader = reader;
        this.processor = processor;
    }

    public void run() {
        String payload = null;

        try {
            while (!shutdown.get()) {
                if ((payload = reader.readLine()) != null) {
                    processor.addEvent(parser.parse(payload));
                    logger.fine(String.format("Added event with payload %s to the processor", payload));
                }
            }
        } catch (InvalidPayloadException e) {
            logger.warning(String.format("Failed to create event with payload %s", payload));
        } catch (IOException e) {
            logger.severe("Failed to read new event");
        }
    }

    @Override
    public void shutdown() {
        super.shutdown();
        try {
            reader.close();
        } catch (IOException e) {
            System.out.println("Failed to close reader for the Event Listener");
        }
    }
}
