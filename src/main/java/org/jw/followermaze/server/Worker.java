package org.jw.followermaze.server;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Worker implements Runnable {
    protected AtomicBoolean shutdown = new AtomicBoolean(false);

    public void shutdown() {
        shutdown.set(true);
    }
}
