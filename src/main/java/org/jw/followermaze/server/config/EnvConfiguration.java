package org.jw.followermaze.server.config;

import java.util.logging.Level;
import java.util.logging.Logger;

public class EnvConfiguration implements Configuration {
    private final static Logger logger = Logger.getLogger(EnvConfiguration.class.getName());

    private int parseInt(String envName, int value) {
        try {
            return Integer.parseInt(System.getenv(envName));
        } catch (NumberFormatException e) {
            logger.warning(String.format("Could not parse %s port from environment", envName));
        }

        return value;
    }

    public int getEventSourcePort() {
        return parseInt(sourcePortConfName, defaultSourcePort);
    }

    public int getClientPort() {
        return parseInt(clientPortConfName, defaultClientPort);
    }

    public Level getLogLevel() {
        try {
            String level = System.getenv(logLevelConfName);
            if (level != null) {
                return Level.parse(level);
            }
        } catch (NullPointerException | IllegalArgumentException e) {
            logger.warning(String.format("Could not parse %s port from environment", logLevelConfName));
        }

        return defaultLogLevel;
    }

    @Override
    public int getMaxEventConnections() {
        return parseInt(maxSourceConConfName, defaultSourceConnections);
    }
}