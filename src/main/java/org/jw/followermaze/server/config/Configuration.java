package org.jw.followermaze.server.config;

import java.util.logging.Level;

public interface Configuration {
    String sourcePortConfName = "eventListenerPort";
    String clientPortConfName = "clientListenerPort";
    String logLevelConfName = "logLevel";
    String maxSourceConConfName = "maxEventConnections";

    int defaultSourcePort = 9090;
    int defaultClientPort = 9099;
    Level defaultLogLevel = Level.INFO;
    int defaultSourceConnections = 1;

    int getEventSourcePort();
    int getClientPort();
    Level getLogLevel();
    int getMaxEventConnections();
}
