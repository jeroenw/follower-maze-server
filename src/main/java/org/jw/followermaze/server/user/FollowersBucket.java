package org.jw.followermaze.server.user;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FollowersBucket {
    private Map<Integer, Set<Integer>> followersMap = new HashMap<>();

    public void addFollower(int userId, int followerId) {
        Set<Integer> followers;

        if (followersMap.containsKey(userId)) {
            followers = followersMap.get(userId);
        } else {
            followers = new HashSet<>();
        }

        followers.add(followerId);
        followersMap.put(userId, followers);
    }

    public void removeFollower(int userId, int followerId) {
        Set<Integer> followers = followersMap.get(userId);

        if (followers == null || followers.isEmpty()){
            return;
        }

        followers.remove(followerId);
        followersMap.put(userId, followers);
    }

    public Set<Integer> getFollowersOf(int userId) {
        return followersMap.get(userId);
    }

    public Map<Integer, Set<Integer>> getAll() {
        return followersMap;
    }
}
