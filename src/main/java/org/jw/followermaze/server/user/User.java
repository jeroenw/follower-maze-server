package org.jw.followermaze.server.user;

import java.io.PrintWriter;
import java.util.logging.Logger;

public class User {
    private final int userId;
    private final PrintWriter writer;

    private final static Logger logger = Logger.getLogger(User.class.getName());

    public User(int userId, PrintWriter writer) {
        this.userId = userId;
        this.writer = writer;
    }

    public int getUserId() {
        return userId;
    }

    public void write(String payload) {
        logger.fine(String.format("Notify user: %s -> %d", payload, userId));
        writer.println(payload);
        if (writer.checkError()) {
            logger.warning(String.format("Write error for %s", payload));
        }
    }
}
