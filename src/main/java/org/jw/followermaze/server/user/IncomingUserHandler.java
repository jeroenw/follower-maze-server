package org.jw.followermaze.server.user;

import org.jw.followermaze.server.ConnectionHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class IncomingUserHandler implements ConnectionHandler {
    private ExecutorService executorService;
    private UserBucket users;

    private final static Logger logger = Logger.getLogger(IncomingUserHandler.class.getName());

    public IncomingUserHandler(UserBucket users) {
        executorService = Executors.newCachedThreadPool();
        this.users = users;
    }

    @Override
    public void handle(Socket socket) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String id = reader.readLine();

                    PrintWriter writer = new PrintWriter(socket.getOutputStream());
                    User user = new User(Integer.parseInt(id), writer);

                    users.add(user);

                    logger.info(String.format("Added a new User with id %s to the UserBucket", id));
                } catch (IOException | NumberFormatException e) {
                    logger.warning("Failed to read the id of a connected user, will discard");
                }
            }
        });
    }

    @Override
    public void shutdown() {
        executorService.shutdown();
    }

    @Override
    public boolean acceptNewConnections() {
        return true;
    }
}
