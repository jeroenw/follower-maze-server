package org.jw.followermaze.server.user;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

public class UserBucket {
    private ConcurrentHashMap<Integer, User> users = new ConcurrentHashMap<>();

    public void add(User user) {
        users.put(user.getUserId(), user);
    }

    public Collection<User> getAll() {
        return users.values();
    }

    public User get(int id) {
        return users.get(id);
    }
}
